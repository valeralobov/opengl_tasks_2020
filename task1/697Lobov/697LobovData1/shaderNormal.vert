#version 330

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;

uniform mat3 normalToCameraMatrix;

layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec3 vertexNormal;

out vec4 color;

void main()
{
    vec4 posCamSpace = viewMatrix * modelMatrix * vec4(vertexPosition, 1.0);
    vec3 viewDirection = normalize(-posCamSpace.xyz);
    vec3 normalCamSpace = normalize(normalToCameraMatrix * vertexNormal);
    if (dot(viewDirection, normalCamSpace) > 0.0) {
        color.rgb = -vertexNormal.xyz * 0.5 + 0.5;
        color.a = 1.0;
    }
    else
    {
        color.rgb = vertexNormal.xyz * 0.5 + 0.5;
        color.a = 1.0;
    }
    gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(vertexPosition, 1.0);
}
