#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>

#include <iostream>
#include <algorithm>

class MoebiusStrip : public Application {
public:
    MoebiusStrip() : _radius(1), _width(0.5), _step(0.001), _dt(0.001), _ds(0.01) {}

    void makeScene() override {
        Application::makeScene();

        std::vector<glm::vec3> vertices;
        std::vector<glm::vec3> normals;

        double t = 0;
        double s = -_width;

        while (t <= 2 * glm::pi<float>()) {
            makePoint(vertices, normals, t, s);
            makePoint(vertices, normals, t + _dt, s);
            s += _ds;
            if (s > _width) {
                t += _dt;
                s = -_width;
            }
        }

        DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());
        buf1->setData(normals.size() * sizeof(float) * 3, normals.data());
        _moebius_strip = std::make_shared<Mesh>();
        _moebius_strip->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
        _moebius_strip->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
        _moebius_strip->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -1.0f, 0.5f)));
        _moebius_strip->setPrimitiveType(GL_POINTS);
        _moebius_strip->setVertexCount(vertices.size());
        _shader = std::make_shared<ShaderProgram>("697LobovData1/shaderNormal.vert", "697LobovData1/shader.frag");
    }

    void update() override {
        Application::update();
    }

    void draw() override {
        Application::draw();

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        _shader->use();

        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        _shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _moebius_strip->modelMatrix()))));

        _shader->setMat4Uniform("modelMatrix", _moebius_strip->modelMatrix());
        _moebius_strip->draw();

    }

    void handleKey(int key, int scancode, int action, int mods) override {
        Application::handleKey(key, scancode, action, mods);

        const double step_t = _step;

        if (action == GLFW_PRESS) {
            if (key == GLFW_KEY_ESCAPE) {
                glfwSetWindowShouldClose(_window, GL_TRUE);
            }
            if (key == GLFW_KEY_MINUS) {
                _dt += step_t;
                makeScene();
            }
            if (key == GLFW_KEY_EQUAL) {
                _dt = std::max(_dt - step_t, 0.001);
                makeScene();
            }
        }

    }

    void makePoint(std::vector<glm::vec3> &vertices, std::vector<glm::vec3> &normals, double t, double s) {
        double x = (_radius + s * cos(0.5 * t)) * cos(t);
        double y = (_radius + s * cos(0.5 * t)) * sin(t);
        double z = s * sin(0.5 * t);

        double x_derivative_by_s = cos(0.5 * t) * cos(t);
        double y_derivative_by_s = sin(t) * cos(0.5 * t);
        double z_derivative_by_s = sin(0.5 * t);

        double x_derivative_by_t = -sin(t) * (_radius + s * cos(0.5 * t)) - 0.5 * s * sin(0.5 * t) * cos(t);
        double y_derivative_by_t = cos(t) * (_radius + s * cos(0.5 * t)) - 0.5 * s * sin(0.5 * t) * sin(t);
        double z_derivative_by_t = 0.5 * s * cos(0.5 * t);

        double normal_x = y_derivative_by_t * z_derivative_by_s - y_derivative_by_s * z_derivative_by_t;
        double normal_y = z_derivative_by_t * x_derivative_by_s - z_derivative_by_s * x_derivative_by_t;
        double normal_z = x_derivative_by_t * y_derivative_by_s - x_derivative_by_s * y_derivative_by_t;

        vertices.push_back({x, y, z});
        normals.push_back({normal_x, normal_y, normal_z});
    }

private:
    MeshPtr _moebius_strip;
    ShaderProgramPtr _shader;

    float _radius;
    float _width;

    float _step;
    float _dt;
    float _ds;
};

int main() {
    MoebiusStrip app;
    app.start();
    return 0;
}